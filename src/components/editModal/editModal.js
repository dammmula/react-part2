import React from "react";
import './editModal.css';

export default class EditModal extends React.Component {
    render() {
        const {editModal, editModalText,
            onModalInput, onEditMessage,
            onCloseModal} = this.props;
        const modalClassName = editModal ? 'edit-message-modal modal-shown' : 'edit-message-modal';

        return (
            <div className={modalClassName}>
                <div className="modal-content border-dark" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div className="modal-header">
                        <h5 className="modal-title" id="staticBackdropLabel">Edit message</h5>
                        <button type="button"
                                className="btn-close"
                                data-dismiss="modal"
                                onClick={onCloseModal}/>
                    </div>
                    <div className="modal-body">
                        <textarea type="text"
                                  value={editModalText}
                                  onChange={onModalInput}
                                  className="form-control edit-message-input"
                                  aria-label="Message"
                                  aria-describedby="button-addon2" />
                    </div>
                    <div className="modal-footer">
                        <button
                            type="button"
                            className="btn btn-primary edit-message-button"
                            onClick={onEditMessage}>
                            OK
                        </button>
                        <button
                            type="button"
                            className="btn btn-secondary edit-message-close"
                            data-dismiss="modal"
                            onClick={onCloseModal}>
                            Cancel
                        </button>
                    </div>
                </div>
            </div>

        )
    }
}