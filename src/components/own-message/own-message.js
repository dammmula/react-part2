import React from "react";
import moment from "moment";
import './own-message.css';

export default class OwnMessage extends React.Component {
    render() {
        const {id, text, onShowEditModal,
            onDeleteMessage, createdAt} = this.props;
        const time = moment(createdAt).format('HH:mm');

        return (
            <div className='own-message'>
                <div className="card border-dark mb-3">
                    <div className="card-body">
                        <h4 className="card-title ">You</h4>
                        <p className="card-text message-text">{text}</p>
                    </div>
                    <div className="card-footer bg-transparent">
                        <span className='buttons'>
                            <i className="fas fa-cog message-edit"
                               onClick={() => onShowEditModal(id)}></i>
                            <i className="fas fa-trash-alt message-delete"
                               onClick={() => onDeleteMessage(id)}></i>
                        </span>
                        <span className='message-time'>{time}</span>
                    </div>
                </div>
            </div>
        )
    }
}