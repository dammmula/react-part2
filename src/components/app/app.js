import React from "react";
import "bootswatch/dist/lux/bootstrap.min.css";
import './app.css';
import Chat from "../chat/chat";

export default class App extends React.Component {
    render() {
        return <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json"/>;
    }
}
