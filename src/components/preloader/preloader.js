import React from "react";
import './preloader.css';

export default class Preloader extends React.Component {
    render() {
        return (
            <div className='preloader'>
                <div className="spinner-border" role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
            </div>
        )
    }
}