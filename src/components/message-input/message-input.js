import React from "react";
import './message-input.css';

export default class MessageInput extends React.Component {

    render() {
        const { text, onMessageInput,
            onMessageSend } = this.props;

        return (
            <div className='message-input card'>
                    <textarea type="text"
                              value={text}
                              onChange={onMessageInput}
                              className="form-control message-input-text"
                              placeholder="Enter your message here..."
                              aria-label="Message"
                              aria-describedby="button-addon2" />
                        <button className="btn btn-outline-secondary message-input-button"
                                type="button"
                                onClick={onMessageSend}>
                            Button
                        </button>
            </div>
        );
    }
}