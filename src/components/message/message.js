import React from "react";
import './message.css';
import moment from "moment";

export default class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLiked: false
        }
    }

    onLikeClick = () => {
        this.setState({
            isLiked: !this.state.isLiked
        })
    }

    formatDate = (str) => {
        const date = moment(str).format('HH:mm');
        return date;
    }

    render() {
        const { avatar, user, text, createdAt, editedAt } = this.props;

        const likeClassName = this.state.isLiked ?
            'message-liked fas fa-heart' :
            'message-like far fa-heart';
        const edited = editedAt ? ' (edited)' : null;
        const time = this.formatDate(createdAt);

        return (
            <div className='message'>
                <div className=''>
                    <img className='message-user-avatar' src={avatar} alt=''/>
                </div>

                <div className="card border-dark mb-3">
                    <div className="card-body">
                        <h4 className="card-title message-user-name">{user}</h4>
                        <p className="card-text message-text">{text}</p>
                    </div>
                    <div className="card-footer bg-transparent">
                        <span className='message-time'>{ time } { edited }</span>
                        <i className={likeClassName} onClick={this.onLikeClick}></i>
                    </div>
                </div>
            </div>
        );
    }
}