import React, {useEffect} from "react";
import Preloader from "../preloader/preloader";
import Header from "../header/header";
import MessageList from "../message-list/message-list";
import MessageInput from "../message-input/message-input";
import EditModal from "../editModal/editModal";
import './chat.css';
import moment from "moment";
import * as actions from "../../store/actions/actions";
import {connect} from "react-redux";


export const Chat = (props) => {
    const {url, saveMessages, deleteMessage,
        inputMessage, sendMessage,
        showEditModal, inputModal,
        editMessage, closeModal,
        editLastMessage} = props;

    const {messages, preloader, messageInput,
        userId, editModal, editModalText,
        messageIds} = props.chat;

    const sortMessages = (messages) => {
        messages.sort(({createdAt: a}, {createdAt: b}) => {
            return moment(a) - moment(b);
        });
        return messages;
    }

    useEffect(() => {
        const fetchData = async () => {
            const res = await fetch(url);

            if (!res.ok) {
                throw new Error(`Could not fetch ${url}, received ${res.status}`);
            }

            const data = await res.json();
            saveMessages(sortMessages(data));
        }

        document.addEventListener('keyup', (event) => {
            if (event.keyCode === 38) {
                editLastMessage();
            }
        })

        fetchData();
    }, []);

    const createMessage = (text) => {
        return {
            text,
            createdAt: moment().format(),
            editedAt: '',
            userId: userId,
            id: messageIds
        }
    }

    const onMessageInput = (event) => {
        const text = event.target.value;
        inputMessage(text);
    }

    const onMessageSend = () => {
        const newMessage = createMessage(messageInput);
        sendMessage(newMessage);
    }

    const onShowEditModal = (id) => {
        showEditModal(id);
    }

    const onEditMessage = () => {
        editMessage();
    }

    const onDeleteMessage = (id) => {
        deleteMessage(id);
    }

    const onModalInput = (event) => {
        const text = event.target.value;
        inputModal(text);
    }

    const onCloseModal = () => {
        closeModal();
    }



    if (preloader) {
        return <Preloader />;
    }
    const chatClassName = editModal ?
        'chat container faded' :
        'chat container';

    return (
        <div className={chatClassName}>
            <Header messages={messages}/>
            <MessageList
                onShowEditModal={onShowEditModal}
                onEditMessage={onEditMessage}
                onDeleteMessage={onDeleteMessage}
                messages={messages}
                userId={userId}/>
            <MessageInput
                text={messageInput}
                onMessageInput={onMessageInput}
                onMessageSend={onMessageSend} />
            <EditModal
                editModal={editModal}
                editModalText={editModalText}
                onEditMessage={onEditMessage}
                onModalInput={onModalInput}
                onCloseModal={onCloseModal}/>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        chat: state.chat
    }
}

const mapDispatchToProps = {
    ...actions
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);