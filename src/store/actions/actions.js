export const SAVE_MESSAGES = 'SAVE_MESSAGES';
export const DELETE_MESSAGE = 'DELETE_MESSAGE';
export const INPUT_MESSAGE = 'INPUT_MESSAGE';
export const SEND_MESSAGE = 'SEND_MESSAGE';
export const SHOW_EDIT_MODAL = 'SHOW_EDIT_MODAL'
export const INPUT_MODAL = 'INPUT_MODAL';
export const EDIT_MESSAGE = 'EDIT_MESSAGE';
export const CLOSE_MODAL = 'CLOSE_MODAL';
export const EDIT_LAST_MESSAGE = 'EDIT_LAST_MESSAGE';


export const DO_SMTH = () => ({
    type: 'DO_SMTH'
})

export const DO_SMTH_ELSE = () => ({
    type: 'DO_SMTH_ELSE'
})

export const saveMessages = (messages) => ({
    type: SAVE_MESSAGES,
    payload: {
        messages
    }
})

export const deleteMessage = (id) => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
})

export const inputMessage = (text) => ({
    type: INPUT_MESSAGE,
    payload: {
        text
    }
})

export const sendMessage = (message) => ({
    type: SEND_MESSAGE,
    payload: {
        message
    }
})

export const showEditModal = (id) => ({
    type: SHOW_EDIT_MODAL,
    payload: {
        id
    }
})

export const inputModal = (text) => ({
    type: INPUT_MODAL,
    payload: {
        text
    }
})

export const editMessage = () => ({
    type: EDIT_MESSAGE
})

export const closeModal = () => ({
    type: CLOSE_MODAL
})

export const editLastMessage = () => ({
    type: EDIT_LAST_MESSAGE
})