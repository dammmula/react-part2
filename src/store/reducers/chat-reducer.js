import {
    CLOSE_MODAL,
    DELETE_MESSAGE, EDIT_LAST_MESSAGE,
    EDIT_MESSAGE,
    INPUT_MESSAGE,
    INPUT_MODAL,
    SAVE_MESSAGES,
    SEND_MESSAGE,
    SHOW_EDIT_MODAL
} from "../actions/actions";

const initialState = {
    "chat": {
        "messages": [],
        "editModal": false,
        "preloader": true,
        "messageInput": "",
        "userId": 1,
        "editModalText": "",
        "editedMessageId": null,
        "messageIds": 1
    }
};

export function reducer(state = initialState, action) {
    let messages;
    let message;

    switch(action.type) {
        case SAVE_MESSAGES:
            return {
                chat: {
                    ...state.chat,
                    messages: action.payload.messages,
                    preloader: false
                }
            }
        case INPUT_MESSAGE:
            return {
                chat: {
                    ...state.chat,
                    messageInput: action.payload.text
                }
            };
        case SEND_MESSAGE:
            message = action.payload.message;
            return {
                chat: {
                    ...state.chat,
                    messages: [...state.chat.messages, message],
                    messageInput: '',
                    messageIds: ++state.chat.messageIds
                }
            }
        case DELETE_MESSAGE:
            messages = state.chat.messages
                .filter((message) => message.id !== action.payload.id);
            return {
                chat: {
                    ...state.chat,
                    messages,
                }
            }
        case SHOW_EDIT_MODAL:
            const text = state.chat.messages
                .find(message => message.id === action.payload.id).text;
            return {
                chat: {
                    ...state.chat,
                    editModal: true,
                    editModalText: text,
                    editedMessageId: action.payload.id
                }
            }
        case INPUT_MODAL:
            return {
                chat: {
                    ...state.chat,
                    editModalText: action.payload.text
                }
            }
        case EDIT_MESSAGE:
            messages = state.chat.messages.map((message) => {
                    if (message.id === state.chat.editedMessageId) {
                        message.text = state.chat.editModalText
                    }
                    return message;
                })
            return {
                chat: {
                    ...state.chat,
                    messages,
                    editModalText: '',
                    editModal: false,
                    editedMessageId: null
                }
            }
        case CLOSE_MODAL:
            return {
                chat: {
                    ...state.chat,
                    editModal: false,
                    editModalText: '',
                    editedMessageId: null
                }
            }
        case EDIT_LAST_MESSAGE:
            message = state.chat.messages[state.chat.messages.length - 1];
            if (message.userId !== state.chat.userId) {
                return state;
            }
            return {
                chat: {
                    ...state.chat,
                    editModal: true,
                    editModalText: message.text,
                    editedMessageId: message.id
                }
            }
        default: return state;
    }
}