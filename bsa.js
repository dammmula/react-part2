import Chat from "./src/components/chat/chat.js";
import {rootReducer} from "./src/store/reducers/root-reducer";


export default {
    Chat,
    rootReducer,
};